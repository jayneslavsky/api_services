import datetime
import base64
import pymysql
import hashlib

from Vigenere import Encode

class RandomKey:


	def toWords(self,n):
		ones = {'0':'zero','1':'one','2':'two','3':'three','4':'four','5':'five','6':'six','7':'seven','8':'eight','9':'nine'}
		teens = {'0':'ten','1':'eleven','2':'twelve','3':'thirteen','4':'fourteen','5':'fifteen','6':'sixteen','7':'seventeen','8':'eighteen','9':'nineteen'}
		tens = {'2':'twenty','3':'thirty','4':'forty','5':'fifty'}

		if(len(n)==1):
			return ones[n[0]]

		else:
			if(n[0]=='1'):
				return teens[n[1]]
			elif(n[1]=='0'):
				return tens[n[0]]
			else:
				return "%s%s" % (tens[n[0]],ones[n[1]])

	def Generate(self,api_key):
		date = datetime.datetime.now()
		hour = self.toWords(str(date.hour))
		minutes = self.toWords(str(date.minute))
		alphabet = "-abcdefghijklmnopqrstuvwxyz"

		key = "%s%s%s%s%s" % (minutes,hour,date.month,date.year,date.day)
		keyword = base64.b64encode(key).lower()
		api_key = api_key.lower()
		cipher1 = Encode(keyword,api_key).cypher(keyword,api_key)
	
		if(len(str(date.minute))==1):
			minute = "0%s" % date.minute
		else:
			minute = date.minute
		if(len(str(date.day))==1):
			day = "0%s" % date.day
		else:
			day = date.day

		year1 = str(date.year)
		year = int(year1[2:4])

		cipher = "%s%s%s%s%s%s" % (cipher1,minute,alphabet[date.hour],alphabet[date.month],alphabet[year],day)

		

		tokenExpire = datetime.datetime.now() + datetime.timedelta(minutes=10)

		conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='dashboard')
		cur = conn.cursor()
		query = "Insert into tokens values('%s','%s')" % (hashlib.sha256(cipher).hexdigest(),tokenExpire)
		cur.execute(query)
		conn.commit()

		return cipher

	def decrypt(self,cipher):
		alphabet = '-abcdefghijklmnopqrstuvwxyz'
		length = len(cipher)
		fromlength = length - 7
		key = cipher[fromlength:length]

		if(key[0:1]=="0"):
			minu = key[1:2]
		else:
			minu = key[0:2]

		minutes = self.toWords(str(minu))
		hour = self.toWords(str(alphabet.index(key[2])))
		month = alphabet.index(key[3])
		year = "20%s" % alphabet.index(key[4])
		if(key[5:6]=="0"):
			day = key[6:7]
		else:
			day = key[5:7]
		keyword = "%s%s%s%s%s" % (minutes,hour,month,year,day)
		keyword = base64.b64encode(keyword).lower()
		message = cipher[0:fromlength]
		api_key = Encode(keyword,message).decypher(keyword,message)


		return api_key

# api_key = RandomKey().decrypt('zz=xdqxjz6pcftj606nbo02');
# print api_key
		
