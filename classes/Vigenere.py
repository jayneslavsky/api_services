class Encode:

	def __init__(self,keyword,message):
		self.alphabet = "acegikmoqsuwy13579=08642zxvtrpnljhfdb"
		self.gen_table = ''
		self.key = ''
		self.keywordLength = len(keyword)
		self.messageLength = len(message)
		self.alphabetLength = len(self.alphabet)

	def generate(self,keyword,message):
		table = ''
		for key in keyword:
			if key not in table:
				table += key

		for a in self.alphabet:
			if a not in table:
				table += a
				
		x = 0
		length = self.alphabetLength - 1
		for x in range(0,5):
			ctr = x
			while ctr <= length:
				self.gen_table += table[ctr]
				ctr += 5

		if(self.messageLength>self.keywordLength):
			length = self.messageLength/self.keywordLength
			self.key = keyword * length

		if(self.messageLength==self.keywordLength):
			self.key = keyword

		remainder = self.messageLength % self.keywordLength
		if remainder:
			self.key += keyword[0:remainder]


	def cypher(self,keyword,message):
		self.generate(keyword,message)
		control = 0
		cipher = ''
		while (control!=self.messageLength):
			x = 0
			while(self.alphabet[x]!=self.key[control]):
				x += 1

			ctr = 0
			while(self.alphabet[ctr]!=message[control]):
				ctr += 1

			z = ctr + x
			length = self.alphabetLength - 1
			if ( z > length):
				z = z - length - 1
			
			cipher += self.gen_table[z]
			control += 1
		return cipher

	def decypher(self,keyword,message):
		self.generate(keyword,message)
		control = 0
		decipher = ''
		while (control!=self.messageLength):
			x = 0
			while(self.alphabet[x]!=self.key[control]):
				x += 1

			ctr = 0
			while(self.gen_table[ctr]!=message[control]):
				ctr += 1
			z = ctr - x
			decipher += self.alphabet[z]
			control += 1

		return decipher
	