import json
import datetime
import pymysql

class Error:
	def ErrorCode(self,code,api_key,service):
		errors = {}

		errors[1] = "Invalid JSON format."

		errors[10] = "API Key does not exist."
		errors[11] = "Not authorized to use this endpoint."
		errors[12] = "Reached the limit of unauthorized access. Access is deactivated."
		errors[13] = "Reached the limit of failed logins. Access is deactivated."
		errors[14] = "Access is deactivated."
		errors[15] = "Invalid token."
		errors[16] = "Access Key expired. Please regenerate."
		errors[17] = "Access expired. Access is deactivated."
		errors[18] = "Key time expired."
		errors[19] = "Lacking parameters."
		errors[20] = "Invalid Student Number."
		errors[21] = "Unauthorized All Access."
		errors[22] = "Token expired."

		date = datetime.datetime.now()

		conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='dashboard')
		cur = conn.cursor()
		query = "Insert into logging values('%s','%s','%s','%s',%s)" % (api_key,errors[code],service,date,code)
		cur.execute(query)
		conn.commit()


		data = {}
		data['Error'] = errors[code]
		data['Error_code'] = code
		data['status'] = "error"

		return json.dumps(data)