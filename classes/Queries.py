import pyodbc

class Queries:

	def __init__(self):
		self.table = {
		    '"': "",
		    "'": "",
		    ",,,": "",
		    "\n": "",
		    ";" : " ",
		    "\"": "",
		    "\x00" : " ",
		    "\r":"",
		    "\xb1":"n"
		}

		self.conn = pyodbc.connect('DRIVER={SQL Server};SERVER=POPO\SQLEXPRESS;DATABASE=must;UID=polens;PWD=polens29')
		self.cursor = self.conn.cursor()

	def enc(self,text):
		if text is not None:
			return "".join(self.table.get(c,c) for c in text)
		else:
			return text

	def MasterList(self):	
		query = "exec masterlist"
		self.cursor.execute(query)
		student = []
		rows = self.cursor.fetchall()
		for row in rows:
			data = {}
			data['StudentNo'] = self.enc(row[0])
			data['Last_name'] = self.enc(row[1]).title()
			data['First_name'] = self.enc(row[2]).title()
			data['Middle_name'] = self.blank(row[3]).title()
			data['Extension_name'] = self.blank(row[4])
			data['Full_name'] = self.enc(row[5]).title()
			student.append(data)

		return student

	def personalInfo(self,studentNo):
		student = []
		if(studentNo!=""):
			query = "exec personalInfoSpec '%s'" % studentNo
		else:
			query = "exec personalInfoAll"

		self.cursor.execute(query)

		rows = self.cursor.fetchall()
		for row in rows:
			data = {}
			data['StudentNo'] = self.enc(row[0])
			data['Last_name'] = self.enc(row[1])
			data['Middle_name'] = self.enc(row[2])
			data['First_name'] = self.enc(row[3])
			data['Extension_name'] = self.blank(row[4])
			data['DateOfBirth'] = self.enc(row[5])
			data['Gender'] = self.enc(row[6])
			data['MobileNo'] = self.blank(row[7])
			data['Height'] = self.blank(row[8])
			data['Weight'] = self.blank(row[9])
			data['BloodType'] = self.blank(row[10])
			data['Res_Address'] = self.blank(row[11])
			data['Res_Street'] = self.blank(row[12])
			data['Res_Barangay'] = self.blank(row[13])
			data['Res_TownCity'] = self.blank(row[14])
			data['Res_ZipCode'] = self.blank(row[15])
			data['Res_Province'] = self.blank(row[16])
			data['Perm_Address'] = self.blank(row[17])
			data['Perm_Street'] = self.blank(row[18])
			data['Perm_Barangay'] = self.blank(row[19])
			data['Perm_TownCity'] = self.blank(row[20])
			data['Perm_ZipCode'] = self.blank(row[21])
			data['Perm_Province'] = self.blank(row[22])

			student.append(data)

		return student

	def guardians(self,studentNo):
		if(studentNo!=""):
			query = "exec guardiansSpec '%s'" % studentNo
		else:
			query = "exec guardiansAll"

		student = []
		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		for row in rows:
			data = {}
			data['StudentNo'] = self.enc(row[0])
			data['Father_Name'] = self.blank(row[1])
			data['Father_Occupation'] = self.blank(row[2])
			data['Father_Company'] = self.blank(row[3])
			data['Father_CompanyAddress'] = self.blank(row[4])
			data['Father_TelNo'] = self.blank(row[5])
			data['Father_Email'] = self.blank(row[6])
			data['Mother_Name'] = self.blank(row[7])
			data['Mother_Occupation'] = self.blank(row[8])
			data['Mother_Company'] = self.blank(row[9])
			data['Mother_CompanyAddress'] = self.blank(row[10])
			data['Mother_TelNo'] = self.blank(row[11])
			data['Mother_Email'] = self.blank(row[12])
			data['Guardian_Name'] = self.blank(row[13])
			data['Guardian_Relationship'] = self.blank(row[14])
			data['Guardian_Address'] = self.blank(row[15])
			data['Guardian_Occupation'] = self.blank(row[16])
			data['Guardian_TelNo'] = self.blank(row[17])
			data['Guardian_Email'] = self.blank(row[16])
			student.append(data)

		return student

	def emergency(self,studentNo):
		if(studentNo!=""):
			query = "exec emergencySpec '%s'" %studentNo
		else:
			query = "exec emergencyAll"

		student = []
		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		for row in rows:
			data = {}
			data['StudentNo'] = self.enc(row[0])
			data['EmergencyContact'] = self.blank(row[1])
			data['Emergency_Address'] = self.blank(row[2])
			data['Emergency_MobileNo'] = self.blank(row[3])
			data['Emergency_TelNo'] = self.blank(row[4])
			data['Student_MobileNo'] = self.blank(row[5])
			data['Student_TelNo'] = self.blank(row[6])
			data['Student_FaxNo'] = self.blank(row[7])
			student.append(data)

		return student

	def numbers(self,studentNo):
		if(studentNo!=""):
			query = "exec numberSpec '%s'" % studentNo
		else:
			query = "exec numberAll"

		student = []
		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		for row in rows:
			data = {}
			data['StudentNo'] = self.enc(row[0])
			data['Mobile_Number'] = self.blank(row[1])
			data['Telephone_Number'] = self.blank(row[2])
			data['Fax_Number'] = self.blank(row[3])
			student.append(data)

		return student

	def education(self,studentNo):
		if(studentNo!=""):
			query = "exec educationSpec '%s'" % studentNo
		else:
			query = "exec educationAll"

		student = []
		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		for row in rows:
			data = {}
			data['StudentNo'] = self.enc(row[0])
			data['Elem_School'] = self.blank(row[1])
			data['Elem_Address'] = self.blank(row[2])
			data['Elem_InclDates'] = self.blank(row[3])
			data['HS_School'] = self.blank(row[4])
			data['HS_Address'] = self.blank(row[5])
			data['HS_InclDates'] = self.blank(row[6])
			data['Vocational'] = self.blank(row[7])
			data['Vocational_Address'] = self.blank(row[8])
			data['Vocational_Degree'] = self.blank(row[9])
			data['Vocational_InclDates'] = self.blank(row[10])
			data['College_School'] = self.blank(row[11])
			data['College_Address'] = self.blank(row[12])
			data['College_InclDates'] = self.blank(row[13])
			data['GS_School'] = self.blank(row[14])
			data['GS_Address'] = self.blank(row[15])
			data['GS_Degree'] = self.blank(row[16])
			data['GS_InclDates'] = self.blank(row[17])
			data['EntranceData_SchoolLastAttended'] = self.blank(row[18])
			data['EntranceData_AdmissionCredential'] = self.blank(row[19])
			data['EntranceData_SchoolLastLocation'] = self.blank(row[20])
			student.append(data)

		return student

	def birthdays(self,studentNo):
		if(studentNo!=""):
			query = "exec birthdaySpec '%s'" % studentNo
		else:
			query = "exec birthdayAll"

		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		student = []
		for row in rows:
			data = {}
			data['StudentNo'] = self.enc(row[0])
			data['Birthday'] = self.blank(row[1])
			student.append(data)

		return student

	def program(self,studentNo):
		if(studentNo!=""):
			query = "exec programSpec '%s'" % studentNo
		else:
			query = "exec programAll"

		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		student = []
		for row in rows:
			data = {}
			data['StudentNo'] = self.enc(row[0])
			data['ProgramName'] = self.enc(row[2])
			data['ProgramShortName'] = self.enc(row[3])
			data['ProgramYears'] = row[4]
			data['ProgramSemesters'] = row[5]
			data['TotalAcademicSubject'] = row[6]
			data['TotalAcademicUnits'] = row[7]
			data['TotalNormalLoad'] = row[8]
			data['TotalGeneralEducationUnits'] = row[9]
			data['TotalMajorUnits'] = row[10]
			data['TotalLectureUnits'] = row[11]
			data['TotalNonLectureUnits'] = row[12]
			student.append(data)

		return student

	def registration(self,studentNo):
		if(studentNo!=''):
			query = "exec registrationSpec '%s'" % studentNo
		else:
			query = "exec registrationAll"

		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		student = []
		for row in rows:
			data = {}
			data['StudentNo'] = row[0]
			data['RegID'] = row[1]
			data['RegDate'] = row[2]
			data['ExpireDate'] = self.blank(row[3])
			data['CampusID'] = row[4]
			data['TermID'] = row[5]
			data['YearLevelID'] = row[6]
			data['CollegeID'] = row[7]
			data['ProgramID'] = row[8]
			data['ProgramName'] = row[9]
			student.append(data)

		return student

	def remarks(self,studentNo):
		if(studentNo!=""):
			query = "exec remarksSpec '%s'" % studentNo
		else:
			query = "exec remarksAll"

		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		student = []
		for row in rows:
			data = {}
			data['StudentNo'] = row[0]
			data['YearLevelID'] = row[1]
			data['RetentionStatus'] = row[2]
			data['RedRemarks'] = self.blank(row[3])
			data['SchoolDelinquencyRemarks'] = self.blank(row[4])
			data['BadAccount'] = row[5]
			student.append(data)

		return student

	def gwa(self,studentNo):
		if(studentNo!=""):
			query = "exec gwaSpec '%s'" % studentNo
		else:
			query = "exec gwaAll"

		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		student = []
		for row in rows:
			data = {}
			data['StudentNo'] = row[0]
			data['YearLevel'] = row[1]
			data['GeneralWeightedAverage'] = row[2]
			data['ProgramName'] = row[3]
			student.append(data)

		return student

	def section(self,studentNo):
		if(studentNo!=""):
			query = "exec sectionSpec '%s'" % studentNo
		else:
			query = "exec sectionAll"

		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		student = []
		for row in rows:
			data = {}
			data['StudentNo'] = row[0]
			data['ClassSectionID'] = row[1]
			data['SectionName'] = row[2]
			data['TermID'] = row[3]
			data['ProgramID'] = row[4]
			data['ProgramName'] = row[5]
			data['YearLevel'] = row[6]
			student.append(data)

		return student

	def grades(self,studentNo):
		if(studentNo!=""):
			query = "exec gradesSpec '%s'" % studentNo
		else:
			query = "exec gradesAll"

		self.cursor.execute(query)
		rows = self.cursor.fetchall()
		student = []
		for row in rows:
			data = {}
			data['StudentNo'] = row[0]
			data['SubjectCode'] = row[1]
			data['SubjectTitle'] = row[2]
			data['Midterm'] = row[3]
			data['Final'] = row[4]
			data['ReExam'] = row[5]
			data['YearLevelID'] = row[6]
			data['FinalRemarks'] = row[7]
			data['AcadUnits'] = row[8]
			data['CreditUnits'] = row[9]

			student.append(data)

		return student

	def blank(self,word):
		if(word=="NULL" or word==None):
			return " "
		else:
			return self.enc(word)
