import smtplib
import sys
import pymysql

app_id = sys.argv[1]
conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='dashboard')
cur = conn.cursor()
cur.execute('call email(%s)',app_id)
for row in cur.fetchall():
	name = row[0]
	email = row[1]

sender = "must.edu.ph"
receiver = "polens29@gmail.com"
message = """From: MUST API Administrator <admin@must.edu.ph>
To: %s <%s>
MIME-Version: 1.0
Content-type: text/html
Subject: Inactive Status

Your application status is inactive. Please contact the administrator.<br/>

All the best,<br/>
Mindanao University of Science and Technology Development Team 
""" % (name,email)

try:
   smtpObj = smtplib.SMTP('localhost')
   smtpObj.sendmail(sender, receiver, message)
except:
   print "Error: unable to send email"