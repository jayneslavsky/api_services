import datetime
import base64
import pymysql
import json
import os
import hashlib

from Vigenere import Encode
from RandomKey import RandomKey
class Validation:

	def __init__(self):
		self.conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='dashboard')
		self.cur = self.conn.cursor()

	def Validate(self,params,service):	


		#CHECK IF TOKEN EXISTS IN PARAMETERS
		if "token" in params:
			tokenparam = base64.b64decode(params['token'])
		else:
			print "no token"
			return (False,19,"Unidentified API key")


		#CHECK IF LENGTH OF TOKEN IS CORRECT
		if(len(tokenparam)!=47):
			return (False,15,"Unidentified API key")

		#CHECK TOKEN PARAM SYMBOLS
		for x in tokenparam:
			if(x==";"):
				return (False,15,"Unidentified API key")
			if(x=='"'):
				return (False,15,"Unidentified API key")

		token = tokenparam[20:47]

		#CHECK IF API KEY EXISTS
		api_key = hashlib.sha256(RandomKey().decrypt(token)).hexdigest()
		self.cur.execute("CALL checkAPIKey(%s)", api_key)
		try:
			app_id = self.cur.fetchall()[0][0]
		except:
			return (False,10,"API Key does not exist.")


		#CHECK IF ACCESS EXPIRED
		self.cur.execute("call datedifference(%s)", app_id)
		for row in self.cur.fetchall():
			if(row[0]<1):
				self.cur.execute('CALL setIsActive(%s)', app_id)
				self.conn.commit()
				return (False,17,app_id)


		#CHECK IF STATUS IS INACTIVE AND CAUSES
		self.cur.execute("CALL statusInactive(%s)", app_id)
		for row in self.cur.fetchall():
			if(row[0]=='3'):
				self.sendmail(app_id)
				return (False,12,app_id)
			elif (row[1]=='1' and row[2]=='3'):
				self.sendmail(app_id)
				return (False,13,app_id)
			elif(row[3]==0):
				self.sendmail(app_id)
				return (False,14,app_id)


		#CHECK IF STUDENT NO. EXISTS IN PARAMETERS
		if "StudentNo" in params:
			StudentNo = params['StudentNo']
		else:
			self.updateFailedLogin(app_id)
			print "no studentno"
			return (False,19,app_id)

		#CHECK IF STUDENT NO LENGTH IS CORRECT
		if(len(StudentNo)!=8):
			if(len(StudentNo)!=10):
				if(len(StudentNo)!=0):
					self.updateFailedLogin(app_id)
					return (False,20,app_id)


		#CHECK FOR SYMBOLS
		for x in StudentNo:
			if(x=='='):
				self.updateFailedLogin(app_id)
				return (False,20,app_id)
			if(x==';'):
				self.updateFailedLogin(app_id)
				return (False,20,app_id)
			if(x=='"'):
				self.updateFailedLogin(app_id)
				return (False,20,app_id)

		
		#CHECK IF ALLOWED ALL ACCESS
		if(service!="getStudentMasterList"):
			if(len(StudentNo)==0):
				self.cur.execute("CALL ApplicationAll(%s)", app_id)
				for row in self.cur.fetchall():
					if(row[11]!=2):
						self.updateUnauthorized(app_id)
						return (False,21,app_id)


		#CHECK IF TIME IS RELEVANT
		time = base64.b64decode(tokenparam[0:20])
		now = datetime.datetime.now()
		lastmin = now - datetime.timedelta(minutes=1)

		now = "%02d%02d%02d%02d%02d" % (now.hour,now.minute,now.month,now.day,now.year)
		lastmin = "%02d%02d%02d%02d%02d" % (lastmin.hour,lastmin.minute,lastmin.month,lastmin.day,lastmin.year)
		newtime = "%s%s" % (time[0:4],time[6:14])

		if(now != newtime):
			if(lastmin != newtime):
				self.updateFailedLogin(app_id)
				return (False,18,app_id)


		#CHECK IF TOKEN EXISTS
		token = hashlib.sha256(token).hexdigest()
		query = "SELECT * from tokens where token = '%s'" % token
		self.cur.execute(query)
		if not self.cur.fetchall():
			self.updateFailedLogin(app_id)
			return (False,22,app_id)



		#CHECK IF APP IS AUTHORIZED TO USE ENDPOINT
		self.cur.execute('call authority(%s,%s)',(app_id,service))
		if not self.cur.fetchall():
			self.updateUnauthorized(app_id)
			return (False,11,app_id)


		return (True,0,app_id)


	def updateFailedLogin(self,app_id):
		self.cur.execute('call ApplicationAll(%s)',app_id)
		for row in self.cur.fetchall():
			if(row[9]=='1'):
				num = int(row[10]) + 1
				deac = int(row[17]) + 1
				self.cur.execute('call updateFailedLogin(%s,%s)',(str(num),app_id))
				self.conn.commit()
				if(num==3):
					self.cur.execute('CALL setIsActive(%s,%s)',(app_id,deac))
					self.conn.commit()
					self.sendmail(app_id)

	def updateUnauthorized(self,app_id):
		self.cur.execute('call ApplicationAll(%s)',app_id)
		for row in self.cur.fetchall():
			num = int(row[7]) + 1
			deac = int(row[17]) + 1

		self.cur.execute('call updateUnauthorized(%s,%s)',(str(num),app_id))
		self.conn.commit()
		print num
		if(num==3):
			self.cur.execute('CALL setIsActive(%s,%s)',(app_id,deac))
			self.conn.commit()
			self.sendmail(app_id)

	def sendmail(self,app_id):
		full_path = os.path.join('classes','SendMail.py')
		os.system('python %s %s' % (full_path,app_id))

	def logging(self,app_id,service):
		date = datetime.datetime.now()
		query = "Insert into logging values ('%s','Retrieved','%s','%s',0)" % (app_id,service,date)
		self.cur.execute(query)
		self.conn.commit()

	#CHECK IF API KEY EXISTS AND TIME LIFE
	def getTokenValidate(self,api_key,time):
		api_key1 = hashlib.sha256(api_key.lower()).hexdigest()
		self.cur.execute("CALL checkAPIKey(%s)", api_key1)
		try:
			app_id = self.cur.fetchall()[0][0]
		except:
			return (False,10)
		

		now = datetime.datetime.now()
		lastmin = now - datetime.timedelta(minutes=1)

		now = "%02d%02d%02d%02d%02d" % (now.hour,now.minute,now.month,now.day,now.year)
		lastmin = "%02d%02d%02d%02d%02d" % (lastmin.hour,lastmin.minute,lastmin.month,lastmin.day,lastmin.year)
		newtime = "%s%s" % (time[0:4],time[6:14])

		if(now != newtime):
			if(lastmin != newtime):
				self.cur.execute('call ApplicationAll(%s)',app_id)
				for row in self.cur.fetchall():
					num = int(row[8]) + 1

				self.cur.execute('call updateFailedLogin(%s,%s)',(str(num),app_id))
				self.conn.commit()
				if(num==3):
					self.cur.execute('CALL setIsActive(%s)',app_id)
					self.conn.commit()
				return (False,18)

		return (True,0)
