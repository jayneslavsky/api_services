from flask import Flask, request, Response
from flask import request
from OpenSSL import SSL
from classes.Validation import Validation
from classes.Error import Error
from classes.RandomKey import RandomKey
from classes.Queries import Queries
import ssl
import json
import base64

app = Flask(__name__)

@app.route('/', methods=['POST'])
def helloworld():
	print request.form
	return "This API works."

@app.route('/getToken/', methods=['POST'])
def getToken():
	try:
		key = base64.b64decode(request.form['key'])
	except:
		return Error().ErrorCode(19,"","getToken")


	if(len(key)!=40):
		return Error().ErrorCode(15,"Unidentified API key","getToken")

	timectr = 39
	api_keyctr = 0
	time = ""
	api_key = ""

	for x in range(0,20):
		time += key[timectr]
		api_key += key[api_keyctr]
		timectr -= 2
		api_keyctr += 2

	isValid, error = Validation().getTokenValidate(api_key,base64.b64decode(time))
	if not isValid:
		return Error().ErrorCode(error,api_key,"getToken")

	token = RandomKey().Generate(api_key)

	return token

@app.route('/getStudentPersonalInfo/', methods=['POST'])
def getStudentPersonalInfo():
	service = "getStudentPersonalInfo"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)


	res = Queries().personalInfo(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.route('/getStudentMasterList/', methods=['POST'])
def getStudentMasterList():
	service = "getStudentMasterList"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().MasterList();

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.route('/getStudentGuardians/', methods=['POST'])
def getStudentGuardians():
	service = "getStudentGuardians"

	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().guardians(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)


@app.route('/getStudentEmergencyContactNumbers/', methods=['POST'])
def getStudentEmergencyContactNumbers():
	service = "getStudentEmergencyContactNumbers"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().emergency(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data) 

@app.route('/getStudentNumbers/', methods=['POST'])
def getStudentNumbers():
	service = "getStudentNumbers"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)


	res = Queries().numbers(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data) 

@app.route('/getStudentEducationalBackground/', methods=['POST'])
def getStudentEducationalBackground():
	service = "getStudentEducationalBackground"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().education(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data) 


@app.route('/getStudentBirthdays/', methods=['POST'])
def getStudentBirthdays():
	service = "getStudentBirthdays"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().birthdays(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.route('/getStudentProgram/', methods=['POST'])
def getStudentProgram():
	service = "getStudentProgram"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().program(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.route('/getStudentRegistrationDetails/', methods=['POST'])
def getStudentRegistrationDetails():
	service = "getStudentRegistrationDetails"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().registration(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.route('/getStudentRemarks/', methods=['POST'])
def getStudentRemarks():
	service = "getStudentRemarks"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().remarks(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.route('/getStudentGWA/', methods=['POST'])
def getStudentGWA():
	service = "getStudentGWA"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().gwa(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.route('/getStudentSection/', methods=['POST'])
def getStudentSection():
	service = "getStudentSection"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().section(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.route('/getStudentGrades/', methods=['POST'])
def getStudentGrades():
	service = "getStudentGrades"
	isValid, error, api_key = Validation().Validate(request.form,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().grades(request.form['StudentNo']);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@app.errorhandler(404)
def page_not_found(e):
	data = {}
	data['status'] = "error"
	data['data'] = "Service not found"

	return json.dumps(data)

@app.errorhandler(500)
def page_not_found(e):
	data = {}
	data['status'] = "error"
	data['data'] = "Internal Server Error"

	return json.dumps(data)