import datetime
import json
import bottle

from bottle import route, run, get, post, request, error, ServerAdapter
from classes.Validation import Validation
from classes.AccessKey import AccessKey
from classes.Error import Error
from classes.Queries import Queries

# @route('/getAccessKey/<params>')
# def getAccessKey(params):
# 	try:
# 		parameters = json.loads(params)
# 	except:
# 		Error().ErrorCode(1,params,'Access Key')

# 	key = AccessKey().key(parameters['api_key'])

# 	data = {}
# 	data['status'] = "ok"
# 	data['AccessKey'] = key

# 	return json.dumps(data)


@route('/test')
def trytest():
	res = Queries().personalInfo("");

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)


#Example url request: /getStudentPersonalInfo/{"api_key":"MTQyMTIwNDA3OHxXZWIgQVBJ","access_key":"m=ohqcojxr1mrfzsb7c13v3w","studentNo":""}

@post('/getStudentPersonalInfo/')
def getStudentInfo():
	service = "getStudentPersonalInfo"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().personalInfo(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@post('/getStudentMasterList/')
def getStudentMasterList():
	service = "getStudentMasterList"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().MasterList();

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@post('/getStudentGuardians/')
def getStudentGuardians():
	service = "getStudentGuardians"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().guardians(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)


@post('/getStudentEmergencyContactNumbers/')
def getStudentEmergencyContactNumbers():
	service = "getStudentEmergencyContactNumbers"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().emergency(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data) 

@post('/getStudentNumbers/')
def getStudentNumbers():
	service = "getStudentNumbers"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().numbers(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data) 

@post('/getStudentEducationalBackground/')
def getStudentEducationalBackground():
	service = "getStudentEducationalBackground"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().education(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data) 


@post('/getStudentBirthdays/')
def getStudentBirthdays():
	service = "getStudentBirthdays"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().birthdays(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@post('/getStudentProgram/')
def getStudentProgram():
	service = "getStudentProgram"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().program(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@post('/getStudentRegistrationDetails/')
def getStudentRegistrationDetails():
	service = "getStudentRegistrationDetails"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().registration(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@post('/getStudentRemarks/')
def getStudentRemarks(params):
	service = "getStudentRemarks"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().remarks(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@post('/getStudentGWA/')
def getStudentGWA():
	service = "getStudentGWA"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().gwa(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@post('/getStudentSection/')
def getStudentSection():
	service = "getStudentSection"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().section(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

run(host='localhost', port=7777, debug=True)

@post('/getStudentGrades/')
def getStudentGrades():
	service = "getStudentGrades"
	api_key = request.forms.get('api_key');
	access_key = request.forms.get('access_key');
	studentNo = request.forms.get('student_number')

	isValid,error = Validation().Validate(api_key,access_key,service)
	if not isValid:
		return Error().ErrorCode(error,api_key,service)

	res = Queries().grades(studentNo);

	Validation().logging(api_key,service)

	data = {}
	data['status'] = "ok"
	data['result'] = res

	return json.dumps(data)

@error(404)
def error404(error):
	data = {}
	data['status'] = "error404"
	data['result'] = "Route not found."



@error(500)
def error500(error):
	data = {}
	data['status'] = "error500"
	data['result'] = "Internal Server Error"

	return json.dumps(data)

# Add our new MySSLCherryPy class to the supported servers
# under the key 'mysslcherrypy'
# reloader restarts it if you update this file
run(host='localhost', port=8080)