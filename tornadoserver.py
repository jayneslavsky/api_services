from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from flaskx import app
import ssl
import os.path

if __name__ == "__main__":
	#http_server = HTTPServer(WSGIContainer(app), ssl_options={"certfile": os.path.join("", "server.crt"),"keyfile": os.path.join("", "server.key")},)
	#http_server = HTTPServer(WSGIContainer(app))

	http_server = HTTPServer(WSGIContainer(app),ssl_options={"certfile": os.path.join("", "server.crt"),"keyfile": os.path.join("", "server.key"),"cert_reqs": ssl.CERT_NONE,"ssl_version": ssl.PROTOCOL_TLSv1})
	http_server.listen(5000)
	print "Engine listening on https://localhost:5000"
	print "...."
	print "...."
	print "...."
	print "...."
	try:
		IOLoop.instance().start()
	except KeyboardInterrupt:
		print "stopping"
		IOLoop.instance().stop()

	# server = HTTPServer(app)
	# server.bind(8888)
	# server.start(0)
	# print "Engine listening on localhost:5000"
	# IOLoop.instance().start()
